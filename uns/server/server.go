package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/headmadepro/uns"
	"bitbucket.org/headmadepro/uns/models"
	"bitbucket.org/headmadepro/uns/resolvers"
	"github.com/99designs/gqlgen/handler"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/gorilla/websocket"
	"github.com/rs/cors"
)

func findUserForRequest(r *http.Request) string {
	return r.Header.Get("Role")
}

func enqueue(message *models.WorkerMessage) error {
	fmt.Printf("%#v", message)
	uns.Enqueue(message)
	return nil
}

func Middleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user := findUserForRequest(r)
			role := user
			ctx := context.WithValue(r.Context(), "role", role)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

const defaultPort = "8080"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}
	allowedOrigins := os.Getenv("ALLOWEDORIGINS")
	if allowedOrigins == "" {
		allowedOrigins = "http://localhost:3000"
	}
	uns.WorkerNew()

	router := chi.NewRouter()
	router.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000", allowedOrigins},
		AllowCredentials: true,
		AllowedHeaders:   []string{"Content-Type", "Role"},
		Debug:            false,
	}).Handler)

	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)

	router.Use(Middleware())

	config := uns.Config{Resolvers: resolvers.NewResolver(enqueue)}
	config.Directives.HasRole = uns.HasRole
	router.Handle("/", handler.Playground("GraphQL", "/query"))
	router.Handle("/query",
		handler.GraphQL(uns.NewExecutableSchema(config),
			handler.WebsocketUpgrader(websocket.Upgrader{
				CheckOrigin: func(r *http.Request) bool {
					return true
				},
			})),
	)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	err := http.ListenAndServe(":"+port, router)
	if err != nil {
		panic(err)
	}
}
