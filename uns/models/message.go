package models

type Message struct {
	ID        int                    `json:"id"`
	UserID    int                    `json:"user_id"`
	Providers []Provider             `json:"providers"`
	Payload   map[string]interface{} `json:"payload"`
}

type WorkerMessage struct {
	Message
	Identities []Identity `json:"identities"`
}

func (m *Message) State() map[string]interface{} {
	return map[string]interface{}{"value": "complated"}
}
