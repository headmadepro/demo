package models

type Identity struct {
	ID       int      `json:"id"`
	UID      string   `json:"uid"`
	Provider Provider `json:"provider"`
	UserID   int      `json:"user_id"`
	Active   bool     `json:"active"`
}
