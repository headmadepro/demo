package models

type User struct {
	ID    int    `json:"id"`
	Title string `json:"title"`
}

func (u *User) Avatar() string {
	return "https://цифровойпрорыв.рф/assets/176fe47dfb13d80fd2858944a93691a0/images/expert.jpg"
}
