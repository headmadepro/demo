package resolvers

import (
	"context"

	"bitbucket.org/headmadepro/uns/models"
)

func (r *queryResolver) Users(ctx context.Context, userID *int) (users []*models.User, err error) {
	if userID != nil {
		for _, user := range r.users {
			if user.ID == *userID {
				users = append(users, user)
				break
			}
		}
	}
	return users, err
}

func (r *userResolver) Identities(ctx context.Context, obj *models.User) (identities []*models.Identity, err error) {
	for _, identity := range r.identities {
		if identity.UserID == obj.ID {
			identities = append(identities, identity)
		}
	}
	return identities, err
}
