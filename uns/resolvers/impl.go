package resolvers

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"time"

	"bitbucket.org/headmadepro/uns/models"
)

type Resolver struct {
	users        []*models.User
	identities   []*models.Identity
	messages     []*models.Message
	messagesChan chan *models.WorkerMessage
}

type EnqueueFunc func(*models.WorkerMessage) error

func NewResolver(enqueue EnqueueFunc) *Resolver {
	r := Resolver{}
	r.messagesChan = make(chan *models.WorkerMessage)
	readJSON(&r.users, "data/users.json")
	readJSON(&r.identities, "data/identities.json")
	readJSON(&r.messages, "data/messages.json")
	go func() {
		for {
			message := <-r.messagesChan
			err := enqueue(message)
			if err != nil {
				go func() {
					time.Sleep(2 * time.Second)
					enqueue(message)
				}()
			}
		}
	}()
	return &r
}

func readJSON(r interface{}, fileName string) {
	f, err := ioutil.ReadFile(fileName)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(f, r); err != nil {
		panic(err)
	}
}
func (r *queryResolver) GetStats(ctx context.Context) (stats *models.Stats, err error) {
	getFromApi("https://uns.headmade.pro/sidekiq/stats", &stats)
	return stats, err
}
