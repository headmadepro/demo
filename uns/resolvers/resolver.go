package resolvers

import (
	"context"

	"bitbucket.org/headmadepro/uns"
	"bitbucket.org/headmadepro/uns/models"
)

// THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

func (r *Resolver) Identity() uns.IdentityResolver {
	return &identityResolver{r}
}
func (r *Resolver) Message() uns.MessageResolver {
	return &messageResolver{r}
}
func (r *Resolver) Mutation() uns.MutationResolver {
	return &mutationResolver{r}
}
func (r *Resolver) Query() uns.QueryResolver {
	return &queryResolver{r}
}
func (r *Resolver) User() uns.UserResolver {
	return &userResolver{r}
}

type identityResolver struct{ *Resolver }

func (r *identityResolver) User(ctx context.Context, obj *models.Identity) (*models.User, error) {
	panic("not implemented")
}

type messageResolver struct{ *Resolver }

type mutationResolver struct{ *Resolver }

type queryResolver struct{ *Resolver }

type userResolver struct{ *Resolver }
