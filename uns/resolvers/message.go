package resolvers

import (
	"context"
	"errors"

	"bitbucket.org/headmadepro/uns/models"
)

func (r *mutationResolver) CreateMessage(ctx context.Context, input models.MessageInput) (*models.Message, error) {
	id := len(r.messages) + 1
	message := &models.Message{UserID: 1, ID: id, Providers: input.Providers, Payload: input.Payload}

	r.messages = append(r.messages, message)
	identities := []models.Identity{}
	for _, identity := range r.identities {
		if identity.UserID == message.UserID {
			identities = append(identities, *identity)
		}
	}
	r.messagesChan <- &models.WorkerMessage{Message: *message, Identities: identities}

	return message, nil
}

func (r *queryResolver) Messages(ctx context.Context, userID *int) (messages []*models.Message, err error) {
	if userID == nil {
		return r.messages, nil
	}
	for _, message := range r.messages {
		if *userID == message.UserID {
			messages = append(messages, message)
		}
	}
	return messages, err
}

func (r *messageResolver) User(ctx context.Context, obj *models.Message) (*models.User, error) {
	for _, user := range r.users {
		if user.ID == obj.UserID {
			return user, nil
		}
	}
	return nil, errors.New("bad user")
}
