package resolvers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func getFromApi(url string, data interface{}) error {
	res, _ := http.Get(url)
	body, _ := ioutil.ReadAll(res.Body)
	if err := json.Unmarshal(body, data); err != nil {
		return err
	}
	return nil
}
