package uns

import (
	"bitbucket.org/headmadepro/uns/models"
	"github.com/jrallison/go-workers"
)

func WorkerNew() {
	workers.Configure(map[string]string{
		// location of redis instance
		"server": "uns-redis:6379",
		// instance of the database
		"database": "2",
		// number of connections to keep open with redis
		"pool": "30",
		// unique process id for this instance of workers (for proper recovery of inprogress jobs on crash)
		"process": "1",
	})
}

func Enqueue(message *models.WorkerMessage) error {
	args := [1]*models.WorkerMessage{message}
	_, err := workers.Enqueue("messages", "MessageWorker", args)
	return err
}
