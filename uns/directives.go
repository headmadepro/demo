package uns

import (
	"context"
	"fmt"

	"bitbucket.org/headmadepro/uns/models"
	"github.com/99designs/gqlgen/graphql"
	"github.com/vektah/gqlparser/gqlerror"
)

func HasRole(ctx context.Context, obj interface{}, next graphql.Resolver, role models.Role) (res interface{}, err error) {
	r := ctx.Value("role")
	fmt.Printf("%#v\n%#v\n\n", obj, ctx)
	if role.IsValid() && r == role.String() {
		return next(ctx)
	}
	ctx.Done()
	return nil, gqlerror.Errorf("Access denied")
}
