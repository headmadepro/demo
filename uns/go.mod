module bitbucket.org/headmadepro/uns

go 1.12

require (
	github.com/99designs/gqlgen v0.9.0
	github.com/alecthomas/gometalinter v3.0.0+incompatible // indirect
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/fatih/motion v0.0.0-20190527122956-41470362fad4 // indirect
	github.com/garyburd/redigo v1.6.0 // indirect
	github.com/google/shlex v0.0.0-20181106134648-c34317bd91bf // indirect
	github.com/josharian/impl v0.0.0-20180228163738-3d0f908298c4 // indirect
	github.com/jrallison/go-workers v0.0.0-20180112190529-dbf81d0b75bb
	github.com/jstemmer/gotags v1.4.1 // indirect
	github.com/kisielk/errcheck v1.2.0 // indirect
	github.com/klauspost/asmfmt v1.2.0 // indirect
	github.com/nsf/gocode v0.0.0-20190302080247-5bee97b48836 // indirect
	github.com/rogpeppe/godef v1.1.1 // indirect
	github.com/vektah/gqlparser v1.1.2
	github.com/zmb3/gogetdoc v0.0.0-20190228002656-b37376c5da6a // indirect
	golang.org/x/crypto v0.0.0-20190611184440-5c40567a22f8 // indirect
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980 // indirect
	golang.org/x/sys v0.0.0-20190614160838-b47fdc937951 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190614205625-5aca471b1d59 // indirect
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20180810215634-df19058c872c // indirect
)
