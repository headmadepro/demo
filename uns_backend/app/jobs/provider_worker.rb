require 'sms_service'

class ProviderWorker < ApplicationWorker
  def log_message(state, params)
    provider = self.class.name.gsub(/Worker$/,'').upcase
    Clickhouse.log_message(params['id'], params['user_id'], provider, Time.now() - 10.second, state)
  end
end
