class SubscribeWorker < ApplicationWorker
  sidekiq_options queue: :subscribe
  def perform(params)
    Rails.logger.info [:perform, self.class.name, params]
  end
end
