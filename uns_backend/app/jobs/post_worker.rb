class PostWorker < ProviderWorker
  sidekiq_options queue: :post
  def perform(params)
    Rails.logger.info [:perform, self.class.name, params]
    log_message(:delivered, params)
  end
end
