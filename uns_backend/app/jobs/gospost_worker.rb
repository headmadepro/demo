class GospostWorker < ApplicationWorker
  sidekiq_options queue: :gospost
  def perform(params)
    Rails.logger.info [:perform, self.class.name, params]
  end
end
