class TelegramWorker < ApplicationWorker
  sidekiq_options queue: :telegramm
  def perform(params)
    Rails.logger.info [:perform, self.class.name, params]
  end
end
