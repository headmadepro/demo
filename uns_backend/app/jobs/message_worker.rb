class MessageWorker < ApplicationWorker
  sidekiq_options queue: :messages
  def perform(*args)
    params = args.first
    providers = params['providers']
    Clickhouse.log_event(:message_new, params['id'],params['user_id'])
    providers.each do |p|
      identity = params['identities'].select{|x| x['active'] && x['provider'] == p}.first
      unless identity
        Clickhouse.log_event("provider_skip_#{p}", params['id'],params['user_id'])
        next
      end

      worker_params = {
        id: params['id'],
        user_id: params['user_id'],
        identity: identity,
        payload: params['payload'],
      }.stringify_keys

      worker_klass = "#{p}Worker".underscore.classify.constantize
      Rails.logger.debug [:worker_klass, worker_klass.to_s, worker_params]

      worker_klass.perform_async(worker_params)

      Clickhouse.log_event("provider_create_#{p}", params['id'],params['user_id'])
    end
  end
end
