require 'sms_service'

class SmsWorker < ProviderWorker
  sidekiq_options queue: :sms
  def perform(params)
    Rails.logger.info [:perform, self.class.name, params]
    mobile = params['identity']['uid']
    uin = '01234567890123456789'
    amount = '3.14'
    pay_before = Date.tomorrow.to_s
    ::SmsService.send_template(:send_penalty_note_with_discount, mobile, {uin: uin, amount: amount, pay_before: pay_before})
    log_message(:sent, params)
  end
end
