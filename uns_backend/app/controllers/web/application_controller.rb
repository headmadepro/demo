class Web::ApplicationController < ::ApplicationController

  def exception(exc)
    logger.info "500: #{exc.message}: " + params.inspect
    render :file => "#{Rails.root}/public/500.html", status: 500, layout: false
  end

end
