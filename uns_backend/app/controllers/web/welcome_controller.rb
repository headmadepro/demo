class Web::WelcomeController < Web::ApplicationController

  def index
    # Rails feature:
    #  "/path.html": format == 'html',
    #  '/path':      format == :html
    if request.format == :html
      redirect_to root_path if params[:path].present? # params[:path] comes from "swallow" rule in routes.
    else
      head :gone
    end
  end
end
