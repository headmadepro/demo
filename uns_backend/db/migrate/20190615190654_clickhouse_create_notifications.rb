class ClickhouseCreateNotifications < ActiveRecord::Migration[5.2]
  def up
    cc = Clickhouse.connection
    if cc
      cc.execute("CREATE DATABASE #{Clickhouse.config_database_name}")

      cc.create_table("#{Clickhouse.config_database_name}.events") do |t|
        t.UUID         :id
        t.uint64       :message_id
        t.uint64       :user_id
        t.date         :created_date
        t.date_time    :created_at
        t.string       :event_name
        t.engine       "MergeTree(created_date, (message_id), 8192)"
      end

      cc.create_table("#{Clickhouse.config_database_name}.messages") do |t|
        t.UUID         :id
        t.uint64       :message_id
        t.uint64       :user_id
        t.string       :provider
        t.date         :created_date
        t.date         :updated_date
        t.date_time    :created_at
        t.date_time    :updated_at
        t.string       :state
        t.float64      :price
        t.engine       "MergeTree(updated_date, (message_id), 8192)"
      end

    else
      puts 'WARNING: no clickhouse connection'
    end
  end

  def down
    cc = Clickhouse.connection
    if cc
      cc.execute("DROP DATABASE #{Clickhouse.config_database_name}")
    else
      puts 'WARNING: no clickhouse connection'
    end
  end
end
