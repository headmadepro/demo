Sidekiq.configure_server do |config|
  config.redis = { url: Rails.application.config.x.redis.sidekiq_url }
end

Sidekiq.configure_client do |config|
  config.redis = { url: Rails.application.config.x.redis.sidekiq_url }
end

#Sidekiq::Extensions.enable_delay!

if Sidekiq.server?
  Sidekiq::Cron::Job.destroy_all!
  # cron_time = '0 0 * * * Europe/Moscow' # Every day at 0:00:00.
  # registry_crons = Rails.application.config.x.registries.crons_info

  # registry_crons.each do |value|
  #   Sidekiq::Cron::Job.create(
  #     name: "Import items for registry #{value[:parser_class_name].demodulize}",
  #     cron: cron_time,
  #     class: Crons::RegistryImportWorker.name,
  #     args: value
  #   )
  # end
end
# puts 'Run "echo flushall | redis-cli" from shell console to cleanup redis (for all projects)' if Rails.env.development?
