#Rails.application.config.session_store :cookie_store,
#  :key => '_uslugi_session',
#  :expire_after => 60.minutes

# From omniauth docs:
#Rails.application.config.middleware.use ActionDispatch::Cookies
#Rails.application.config.middleware.use ActionDispatch::Session::CookieStore, Rails.application.config.session_options


Rails.application.config.session_store :redis_store,
  :servers => Rails.application.config.x.redis.sessions_url,
  :expire_after => Rails.application.config.x.session.expire_after
