module Clickhouse
  def self.config_database_name() Rails.application.config.x.clickhouse.database_name ; end

  def self.log_event(event_name,message_id,user_id)
    Clickhouse.connection.insert_rows(
      "#{Clickhouse.config_database_name}.events",
      names: %w(id message_id user_id created_date created_at event_name)
    ) do |rows|
      rows << [
        SecureRandom.uuid,
        message_id,
        user_id,
        Date.today,
        format_datetime(Time.now),
        event_name
      ]
    end
  end

  def self.format_datetime(time)
    time.to_s.split(' ')[0..-2].join(' ')
  end

  def self.log_message(message_id,user_id,provider,created_at,state)
    Clickhouse.connection.insert_rows(
      "#{Clickhouse.config_database_name}.messages",
      names: %w(id message_id user_id provider created_date updated_date created_at updated_at state price)
    ) do |rows|
      price = {
        'SMS' => 2,
        'POST' => 80,
        'GOSPOST' => 5,
      }[provider] || 0

      rows << [
        SecureRandom.uuid,
        message_id,
        user_id,
        provider,
        created_at.to_date,
        Date.today,
        format_datetime(created_at),
        format_datetime(Time.now),
        state,
        price.to_f,
      ]
    end
  end
end

if Rails.application.config.x.clickhouse.connection.url.present?
  Clickhouse.establish_connection(Rails.application.config.x.clickhouse.connection)
  #Clickhouse.logger = Rails.logger
end
