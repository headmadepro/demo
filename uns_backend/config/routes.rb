require 'sidekiq/web'
require 'sidekiq/cron/web'

Rails.application.routes.draw do

  namespace :api, defaults: {format: :json} do

  end

  scope module: :web, defaults: {format: :html} do
    root 'welcome#index'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_key_base]
  mount Sidekiq::Web => '/sidekiq' #, :constraints => AdminConstraint.new

  health_check_routes

  get "/*path", to: redirect('/')

end
