require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module UnsBackend
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    config.i18n.default_locale = :ru
    config.active_record.schema_format = :sql

    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += %W(#{config.root}/lib/sms_providers)

    # Enable serving of images, stylesheets, and JavaScripts from an asset server.
    if ENV['ASSET_HOST'].present?
      config.action_controller.asset_host = ENV['ASSET_HOST']
    end

    config.x = Hashie::Mash.new(Rails.application.config_for(:settings))
    config.time_zone = ENV['TIME_ZONE'] || raise('TIME_ZONE env is not set')

    config.active_job.queue_adapter = :sidekiq


    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end
