require 'faraday'

class SmsProviders::SmsRu

  def send_message(phone, message_text, options={})

    Rails.logger.debug([:send, phone, message_text])
    unless phone.starts_with?('+79') || phone.starts_with?('89')
      Rails.logger.info([:send, 'invalid phone', phone, message_text])
      return nil
    end

    settings = Rails.application.config.x.sms_providers.sms_ru

    phone = settings.debug_to if settings.debug_to.present?

    params = {
      api_id: settings.api_id,
      from: settings.from,
      to: phone,
      text: message_text,
    }

    conn = Faraday.new(:url => 'http://sms.ru/sms/') do |c|
      c.use Faraday::Response::RaiseError
      c.use Faraday::Adapter::NetHttp
    end

    response = conn.get do |req|
      req.url 'send'
      req.params = params
    end

    err_code = response.body.split("\n")[0]
    if err_code == '100'
      nil
    else
      Rails.logger.info "Sms.ru: Error code: #{err_code}"
      err_code
    end
  end
end
