require 'faraday'

class SmsProviders::IqsmsRu
  def send_message(phone, message_text, options={})
    Rails.logger.debug([:send, phone, message_text])
    unless phone.starts_with?('+79') || phone.starts_with?('89')
      Rails.logger.info([:send, 'invalid phone', phone, message_text])
      return nil
    end

    settings = Rails.application.config.x.sms_providers.iqsms_ru

    phone = settings.debug_to if settings.debug_to.present?

    params = {
      phone: phone,
      text: message_text,
      sender: settings.from,
    }

    send_params = params.merge({
      login: settings.login,
      password: settings.password,
    })

    conn = Faraday.new(url: settings.url) do |c|
      c.use Faraday::Response::RaiseError
      c.use Faraday::Adapter::NetHttp
    end

    response = conn.get do |req|
      req.url 'send'
      req.params = send_params
    end

    status = response.body.split(';').last
    if status.to_i.to_s != status
      Rails.logger.info( [:iqsms, :send_message, :error, status, settings.url, send_params.inspect])
      raise StandardError.new("Iqsms: #{status}: #{settings.url} #{params.to_h.inspect}")
    end
  end
end
