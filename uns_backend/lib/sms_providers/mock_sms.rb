class SmsProviders::MockSms

  def send_message(phone, message, options={})
    Rails.logger.info([:mock_sms, :send_message, phone, message, options])
  end

end
