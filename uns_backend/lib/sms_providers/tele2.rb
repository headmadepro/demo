require 'faraday'

class SmsProviders::Tele2

  def send_message(phone, message, options={})
    msisdn = phone.gsub('+','')
    params = {
      operation: configus.sms.tele2.operation,
      login: configus.sms.tele2.login,
      password: configus.sms.tele2.password,
      shortcode: configus.sms.tele2.from,
      msisdn: msisdn,
      text: message,
    }

    conn = Faraday.new(url: configus.sms.tele2.url) do |c|
      c.use Faraday::Response::RaiseError
      c.use Faraday::Adapter::NetHttp
    end

    response = conn.get do |req|
      req.url 'send'
      req.params = params
    end

    status = response.body
    if status.to_i.to_s != status
      Rails.logger.info([:tele2, :send_message, :error, status, configus.sms.tele2.url, params.inspect])
      raise StandardError.new("Tele2: #{status}: #{configus.sms.tele2.url} #{params.inspect}")
    end
  end
end
