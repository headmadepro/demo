require 'faraday'

class SmsProviders::HttpSmpp

  def send_message(phone, message_text, options={})

    Rails.logger.debug([:send, phone, message_text])
    unless phone.starts_with?('+79') || phone.starts_with?('89')
      Rails.logger.info([:send, 'invalid phone', phone, message_text])
      return nil
    end
    phone[0] = '' if phone[0] == '+'

    settings = Rails.application.config.x.sms_providers.http_smpp

    phone = settings.debug_to if settings.debug_to.present?

    params = {
      src: settings.src,
      dst: phone,
      text: message_text,
      #register: ''
    }

    conn = Faraday.new(:url => settings.server_url) do |c|
      #c.use Faraday::Response::RaiseError
      c.use Faraday::Adapter::NetHttp
    end

    p conn

    response = conn.get do |req|
      #req.url 'send'
      req.params = params
    end

    # TODO: check/warn if error!
    Rails.logger.info [:response, response.body, response]

  end
end
