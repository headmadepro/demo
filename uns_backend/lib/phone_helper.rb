module PhoneHelper
  def normalize_mobile(phone)
    return phone unless phone.present?
    phone = phone.gsub(/[^+\d]/, '')
    phone = ['+7', phone].join if phone.length == 10 && phone =~ /^9/
    phone = phone.sub(/^89/,'+79') if phone =~ /^89/
    phone
  end
end
