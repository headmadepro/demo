class SmsService
  extend PhoneHelper

  class << self
    SMS_TEMPLATES = {
      send_penalty_note_with_discount: 'Штраф УИН <%=uin%>, сумма <%=amount%>₽, 50% до <%=pay_before%>',
      send_penalty_note: 'Штраф УИН <%=uin%>, сумма <%=amount%>₽',
    }.freeze

    def send_template(name, phone, params)
      send_message(phone, prepare_text(name, params))
    end

    #def method_missing(name, *args, &block)
      #if SMS_TEMPLATES[name]
        #send_template(name, args[0], args[1])
      #else
        #super
      #end
    #end


    private

    def send_message(phones, message, options={})
      #Rails.logger.debug [:send_message, phones, message, options]
      return unless message.present? && phones.present?
      phones = [phones.to_s] unless phones.is_a?(Array)
      message_trimmed = trim(message)
      phones.compact.uniq.map { |phone| provider().send_message(normalize_mobile(phone), message_trimmed, options) }.compact.first
    end

    def trim(message, max_length = nil)
      max_length ||= Rails.application.config.x.sms_service.sms_max_length
      if max_length > 0 && max_length < message.length
        message[0, max_length - 1] + '…'
      else
        message
      end
    end

    def prepare_text(template_name, params)
      raise "Unknown SMS_TEMPLATE: #{template_name}" unless SMS_TEMPLATES[template_name].present?
      #template % params
      ERB.new(SMS_TEMPLATES[template_name]).result_with_hash(params)
    end

    def provider
      @provider ||= begin
        klass = "SmsProviders::#{Rails.application.config.x.sms_service.provider.classify}".safe_constantize
        klass.respond_to?(:new) ? klass.new : SmsProviders::MockSms.new
      end
    end
  end

end
