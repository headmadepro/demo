/**
 *
 * HomePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose, Mutation } from 'react-apollo';
import { Empty, Form, Input, Select, Button } from 'antd';

import { selectProviders } from './query.graphql';
import CREATE_MESSAGE from './createMessage.graphql';

export class HomePage extends React.Component {
  handleSubmit = func => e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        func({ variables: values });
      }
    });
  };

  onCompleted = () => {
    this.props.form.resetFields();
  };

  render() {
    const { loading, error, __type } = this.props.data;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };

    if (error) {
      return error;
    }
    if (loading) {
      return <Empty />;
    }
    const { enumValues } = __type;
    const { getFieldDecorator } = this.props.form;
    return (
      <Mutation mutation={CREATE_MESSAGE} onCompleted={this.onCompleted}>
        {createMessage => (
          <Form {...formItemLayout} onSubmit={this.handleSubmit(createMessage)}>
            <Form.Item label="Телефон">
              {getFieldDecorator('phone')(<Input />)}
            </Form.Item>
            <Form.Item label="Адрес">
              {getFieldDecorator('address')(<Input />)}
            </Form.Item>
            <Form.Item label="ИНН">
              {getFieldDecorator('inn')(<Input />)}
            </Form.Item>
            <Form.Item label="СНИЛС">
              {getFieldDecorator('snils')(<Input />)}
            </Form.Item>
            <Form.Item label="УИН">
              {getFieldDecorator('uin', {
                rules: [
                  { required: true, message: 'Please input your note!' },
                  {
                    pattern: /^\d{20}$/,
                    message: 'Поле должно состоять из 20 цифр',
                  },
                ],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Сумма штрафа">
              {getFieldDecorator('amount', {
                rules: [{ required: true, message: 'Please input your note!' }],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Тип и приоритет доставки">
              {getFieldDecorator('providers', { rules: [{ required: true }] })(
                <Select
                  mode="multiple"
                  placeholder="Please select favourite colors"
                >
                  {enumValues.map(value => (
                    <Select.Option key={value.name} value={value.name}>
                      {value.name}
                    </Select.Option>
                  ))}
                </Select>,
              )}
            </Form.Item>
            <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
              <Button type="primary" htmlType="submit">
                Отправить
              </Button>
            </Form.Item>
          </Form>
        )}
      </Mutation>
    );
  }
}

HomePage.propTypes = {
  data: PropTypes.object,
  form: PropTypes.object,
};

export default compose(
  graphql(selectProviders, {
    options: {
      fetchPolicy: 'network-only',
      // pollInterval: 2000,
    },
  }),
  Form.create({ name: 'validate_other' }),
)(HomePage);
