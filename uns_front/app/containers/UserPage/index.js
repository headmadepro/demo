/**
 *
 * UserPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
import { graphql, compose } from 'react-apollo';
import {
  Avatar,
  PageHeader,
  Tag,
  // Tabs,
  Table,
  Button,
  // Row,
  // Col,
  Empty,
} from 'antd';

import { selectMessages } from './query.graphql';
import { columns } from '../MessagePage';

export class UserPage extends React.Component {
  render() {
    const { loading, error, messages, users } = this.props.data;
    if (error) {
      return error;
    }
    if (loading) {
      return <Empty />;
    }
    const user = users.pop();
    return (
      <div>
        <Helmet>
          <title>UserPage</title>
          <meta name="description" content="Description of UserPage" />
        </Helmet>
        {user && (
          <PageHeader
            onBack={() => window.history.back()}
            title={<Avatar shape="square" src={user.avatar} />}
            subTitle={user.title}
            tags={<Tag color="red">Непрочитанные уведомлений</Tag>}
            extra={[
              <Button key="3">Operation</Button>,
              <Button key="2">Operation</Button>,
              <Button key="1" type="primary">
                Primary
              </Button>,
            ]}
          />
        )}
        <Table
          loading={loading}
          rowKey="id"
          columns={columns.filter(column => column.key !== 'user')}
          dataSource={messages}
        />
      </div>
    );
  }
}

UserPage.propTypes = {
  data: PropTypes.object,
};

export default compose(
  graphql(selectMessages, {
    options: props => ({
      fetchPolicy: 'no-cache',
      variables: {
        user_id: props.match.params.user_id,
      },
    }),
  }),
)(UserPage);
