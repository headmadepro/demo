/**
 *
 * MessagePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { Query, graphql, compose } from 'react-apollo';
import {
  Statistic,
  Row,
  Col,
  Table,
  Tag,
  Popover,
  Empty,
  Skeleton,
} from 'antd';

import langMessages from './messages';
import { selectMessages, stats } from './query.graphql';

export const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Пользователь',
    dataIndex: 'user',
    key: 'user',
    render: user => (
      <Popover
        content={
          <div style={{ width: '400px' }}>
            <Skeleton avatar paragraph={{ rows: 4 }} />
          </div>
        }
        title="Информация о пользователе"
      >
        <Link to={`/users/${user.id}`}>{user.id}</Link>
      </Popover>
    ),
  },
  {
    title: 'Иноформация',
    dataIndex: 'payload',
    key: 'payload',
    render: payload => (
      <p>
        от: {payload.from}, куда: {payload.to}, текст: {payload.text}
      </p>
    ),
  },
  {
    title: 'Способ доставки',
    dataIndex: 'providers',
    key: 'providers',
    render: providers => (
      <span>
        {providers.map(provider => (
          <Tag key={provider}>{provider}</Tag>
        ))}
      </span>
    ),
  },
  {
    title: 'Состояние',
    dataIndex: 'state',
    key: 'state',
    render: state => <Tag color="geekblue">{state.value}</Tag>,
  },
];

export function MessagePage(props) {
  const { loading, error, messages } = props.data;
  if (error) {
    return error;
  }
  if (loading) {
    return <Empty />;
  }
  return (
    <div>
      <Query query={stats} pollInterval={2000}>
        {({ loading, data }) => {
          if (loading) {
            return 'Loading...';
          }
          console.log(data);
          const stat = data.getStats;
          return (
            <Row gutter={16}>
              <Col span={4}>
                <Statistic title="В очереди" value={stat.sidekiq.enqueued} />
              </Col>
              <Col span={4}>
                <Statistic title="В работе" value={stat.sidekiq.busy} />
              </Col>
              <Col span={4}>
                <Statistic title="Завершено" value={stat.sidekiq.processed} />
              </Col>
              <Col span={4}>
                <Statistic title="Провалено" value={stat.sidekiq.failed} />
              </Col>
              <Col span={4}>
                <Statistic
                  title="Uptime(Дней)"
                  value={stat.redis.uptime_in_days}
                />
              </Col>
              <Col span={4}>
                <Statistic
                  title="Использование памяти"
                  value={stat.redis.used_memory_human}
                />
              </Col>
            </Row>
          );
        }}
      </Query>
      <br />
      <Table
        loading={loading}
        rowKey="id"
        columns={columns}
        dataSource={messages}
      />
      <FormattedMessage {...langMessages.header} />
    </div>
  );
}

MessagePage.propTypes = {
  data: PropTypes.object,
};

export default compose(
  graphql(selectMessages, {
    options: props => ({
      fetchPolicy: 'network-only',
      variables: {
        user_id: props.match.params.user_id,
      },
    }),
  }),
)(MessagePage);
