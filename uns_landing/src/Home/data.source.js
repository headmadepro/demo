import React from 'react';
export const Nav10DataSource = {
  wrapper: { className: 'header1 home-page-wrapper' },
  page: { className: 'home-page' },
  logo: {
    className: 'header1-logo jwy30ykeme-editor_css',
    //children: 'http://headmade.pro/static/headmade.9577d2aa.png',
    children: 'https://xn--b1aaqfxbbhefb3bya5f.xn--p1ai/assets/176fe47dfb13d80fd2858944a93691a0/themes/default/assets/images/logo.svg',
  },
  Menu: {
    className: 'header1-menu',
    children: [
      {
        name: 'item0',
        a: {
          children: (
            <>
              <p>1</p>
            </>
          ),
          href: '',
        },
      },
      {
        name: 'item1',
        a: {
          children: (
            <>
              <p>2</p>
            </>
          ),
          href: '',
        },
      },
      {
        name: 'item2',
        a: {
          children: (
            <>
              <p>3</p>
            </>
          ),
          href: '',
        },
      },
    ],
  },
  mobileMenu: { className: 'header1-mobile-menu' },
  help: {
    className: 'help',
    children: (
        <p>ЧаВо</p>
    ),
  },
  user: {},
};
export const Content00DataSource = {
  wrapper: { className: 'home-page-wrapper content0-wrapper' },
  page: { className: 'home-page content0 jwy3ndqqi3-editor_css' },
  OverPack: { playScale: 0.3, className: '' },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      {
        name: 'title',
        children: (
          <>
            <p>Универсальный сервис уведомлений</p>
          </>
        ),
        className: 'jwy2ykr64r-editor_css',
      },
      {
        name: 'title2',
        children: (
          <>
            <p>Каналы получения уведомлений</p>
          </>
        ),
      },
    ],
  },
  block: {
    className: 'block-wrapper',
    children: [
      {
        name: 'block0',
        className: 'block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'icon',
            children:
              'https://zos.alipayobjects.com/rmsportal/YPMsLQuCEXtuEkmXTTdk.png',
          },
          title: {
            className: 'content0-title',
            children: (
              <>
                <p>web-интерфейс</p>
              </>
            ),
          },
          content: {
            children: (
              <>
                <p>интегрированный кабинет</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block1',
        className: 'block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'icon',
            children:
              'https://zos.alipayobjects.com/rmsportal/WBnVOjtIlGWbzyQivuyq.png',
          },
          title: {
            className: 'content0-title',
            children: (
              <>
                <p>API</p>
              </>
            ),
          },
          content: {
            children: (
              <>
                <p>для интеграции со сторонними системами</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block2',
        className: 'block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'icon',
            children:
              'https://zos.alipayobjects.com/rmsportal/EkXWVvAaFJKCzhMmQYiX.png',
          },
          title: {
            className: 'content0-title',
            children: (
              <>
                <p>СМЭВ</p>
              </>
            ),
          },
          content: {
            children: (
              <>
                <p>штрафы из ГИС ГМП</p>
              </>
            ),
          },
        },
      },
    ],
  },
};
export const Content10DataSource = {
  wrapper: { className: 'home-page-wrapper content1-wrapper' },
  OverPack: { className: 'home-page content1', playScale: 0.3 },
  imgWrapper: { className: 'content1-img', md: 10, xs: 24 },
  img: {
    children: 'https://zos.alipayobjects.com/rmsportal/nLzbeGQLPyBJoli.png',
  },
  textWrapper: { className: 'content1-text', md: 14, xs: 24 },
  title: {
    className: 'content1-title',
    children: (
      <>
        <p>Обработка уведомления</p>
      </>
    ),
  },
  content: {
    className: 'content1-content',
    children: (
      <>
        <p>
          Система анализирует поступившее уведомление и выбирает подходящие
          каналы его доставки
        </p>
      </>
    ),
  },
};
export const Content30DataSource = {
  wrapper: { className: 'home-page-wrapper content3-wrapper' },
  page: { className: 'home-page content3' },
  OverPack: { playScale: 0.3 },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      {
        name: 'title',
        children: (
          <>
            <p>Каналы доставки</p>
          </>
        ),
        className: 'title-h1',
      },
      {
        name: 'content',
        className: 'title-content',
        children: (
          <>
            <p>
              Канал доставки может быть явно указан во входном уведомлении&nbsp;
            </p>
            <p>или будет выбран автоматически</p>
          </>
        ),
      },
    ],
  },
  block: {
    className: 'content3-block-wrapper',
    children: [
      {
        name: 'block0',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children:
              'https://zos.alipayobjects.com/rmsportal/ScHBSdwpTkAHZkJ.png',
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title',
            children: (
              <>
                <p>ЛК</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>Интегрированный ЛК гражданина</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block1',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children:
              'https://zos.alipayobjects.com/rmsportal/NKBELAOuuKbofDD.png',
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title',
            children: (
              <>
                <p>Госпочта</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>Отправка штатными средствами Госпочты</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block2',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children:
              'https://zos.alipayobjects.com/rmsportal/xMSBjgxBhKfyMWX.png',
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title',
            children: (
              <>
                <p>Почта РФ</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>Отправка с помощью сервиса электронных сообщений Почты РФ</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block3',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children:
              'https://zos.alipayobjects.com/rmsportal/MNdlBNhmDBLuzqp.png',
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title',
            children: (
              <>
                <p>ЕЛК</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>В единый личный кабинет на портал Госуслуги.ру</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block4',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children:
              'https://zos.alipayobjects.com/rmsportal/UsUmoBRyLvkIQeO.png',
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title',
            children: (
              <>
                <p>SMS, email</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>самые обычные способы доставки&nbsp;</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block5',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children:
              'https://zos.alipayobjects.com/rmsportal/ipwaQLBLflRfUrg.png',
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title',
            children: (
              <>
                <p>API</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>в другие информационные системы "по подписке"</p>
              </>
            ),
          },
        },
      },
    ],
  },
};
export const Feature40DataSource = {
  wrapper: { className: 'home-page-wrapper content6-wrapper' },
  OverPack: { className: 'home-page content6' },
  textWrapper: { className: 'content6-text', xs: 24, md: 10 },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      {
        name: 'title',
        children: (
          <>
            <p>Аналитические инструменты</p>
          </>
        ),
        className: 'title-h1',
      },
      {
        name: 'content',
        className: 'title-content',
        children: (
          <>
            <p>для отчётов, статистики и оперативного управления</p>
          </>
        ),
      },
    ],
  },
  img: {
    children: 'https://zos.alipayobjects.com/rmsportal/VHGOVdYyBwuyqCx.png',
    className: 'content6-img jwy3jw5c12-editor_css',
    xs: 24,
    md: 14,
  },
  block: {
    children: [
      {
        name: 'block0',
        img: {
          children:
            'https://zos.alipayobjects.com/rmsportal/NKBELAOuuKbofDD.png',
          className: 'content6-icon',
        },
        title: {
          className: 'content6-title',
          children: (
            <>
              <p>Отправленные уведомления</p>
            </>
          ),
        },
        content: {
          className: 'content6-content',
          children: (
            <>
              <p>В разрезе даты, каналов поступления/доставки и получателя</p>
            </>
          ),
        },
      },
      {
        name: 'block1',
        img: {
          className: 'content6-icon',
          children:
            'https://zos.alipayobjects.com/rmsportal/xMSBjgxBhKfyMWX.png',
        },
        title: {
          className: 'content6-title',
          children: (
            <>
              <p>Статистика использования системы</p>
            </>
          ),
        },
        content: {
          className: 'content6-content',
          children: (
            <>
              <p>Количество отправленных сообщений</p>
            </>
          ),
        },
      },
      {
        name: 'block2',
        img: {
          className: 'content6-icon',
          children:
            'https://zos.alipayobjects.com/rmsportal/MNdlBNhmDBLuzqp.png',
        },
        title: {
          className: 'content6-title',
          children: (
            <>
              <p>Оперативное управление</p>
            </>
          ),
        },
        content: {
          className: 'content6-content',
          children: (
            <>
              <p>
                Текущее состояние: состояние уведомлений, находящихся в процессе
                доставки (где что застряло)
              </p>
            </>
          ),
        },
      },
    ],
  },
};
export const Footer00DataSource = {
  wrapper: { className: 'home-page-wrapper footer0-wrapper' },
  OverPack: { className: 'home-page footer0', playScale: 0.05 },
  copyright: {
    className: 'copyright',
    children: (
      <>
        <span>UNS, Generated by <a href="http://headmade.pro">Genesis</a>, All rights reserved, ©2019</span>
      </>
    ),
  },
};
